//
//  ViewController.swift
//  Topic du jour
//
//  Created by Nirmal Govind on 2/15/19.
//  Copyright © 2019 Nirmal Govind. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    let topics = [
    ("Successes", "appicon-successes"),
    ("Failures", "appicon-failures"),
    ("Feedback", "appicon-feedback"),
    ("Challenges", "appicon-challenges"),
    ("StudioProd", "appicon-studioprod"),
    ("Leadership", "appicon-leadership"),
    ("Technical", "appicon-technical"),
    ("Caitlin", "appicon-manager"),
    ("CP", "appicon-creativeprod"),
    ("StudioFinance", "appicon-studiofin"),
    ("Recruiting", "appicon-recruiting"),
    ("Partnerships", "appicon-partnerships"),
    ("Directs", "appicon-directs"),
    ("Time-management", "appicon-timemgmt"),
    ("Keeper Test", "appicon-keepertest"),
    ("Business/team/impact review", "appicon-impact")
    ]
    
    let day:TimeInterval = 86400.0
    
    @IBOutlet weak var todayDateLabel: UILabel!
    
    @IBOutlet weak var todayTopicLabel: UILabel!
    
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert]) {
            (granted, error) in
            if granted {
                print("yes")
            } else {
                print("No")
            }
        }
     
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.gray.cgColor
        textView.autocorrectionType = .yes
        
        let (date, topicIndex) = getTodayDateTopic()
        let topicName = topics[topicIndex].0
        let topicIconName = topics[topicIndex].1
        
        print(topicIconName)
        
        todayDateLabel.text = date
        todayTopicLabel.text = topicName
        
        // TODO: refactor notification code below -- can't do dynamic content for local notifications
        let content = UNMutableNotificationContent()
//        content.title = "Today's Topic: " + topic
        content.body = "Reflect on today's Topic"
        
        // times for notifications as tuples
        let notificationTimes = [(8, 15), (21, 15)]

        // Configure the recurring date.
        var dateComponents = DateComponents()
        dateComponents.calendar = Calendar.current

        // multiple triggers and requests for notification times
        for eventTime in notificationTimes {
            //        dateComponents.weekday = 7  // Tuesday
            dateComponents.hour = eventTime.0
            dateComponents.minute = eventTime.1

            //         Create the trigger as a repeating event.
            let trigger = UNCalendarNotificationTrigger(
                dateMatching: dateComponents, repeats: true)

            //        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)

            // Create the request
            let uuidString = UUID().uuidString
            let request = UNNotificationRequest(identifier: uuidString,
                                                  content: content, trigger: trigger)

            // Schedule the request with the system.
            let notificationCenter = UNUserNotificationCenter.current()
            notificationCenter.add(request) { (error) in
                if error != nil {
                    // Handle any errors.
                }
            }
        }

//        listScheduledNotifications()
    }

    func listScheduledNotifications()
    {
        UNUserNotificationCenter.current().getPendingNotificationRequests { notifications in
            
            for notification in notifications {
                print(notification)
            }
        }
    }
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        
//        print(textView.text)
        
        let (date, topicIndex) = getTodayDateTopic()
        
        let ulyssesEntry = "#\(date)" + "\n\n" + topics[topicIndex].0 + "\n\n" + textView.text
        
        let escapedText = ulyssesEntry.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let ulyssesURL = "ulysses://x-callback-url/new-sheet?group=TdJ&text=" + escapedText!

//        print(ulyssesURL)
        let (_, nextTopicIndex) = getTodayDateTopic(daysOffset: 1)
        
        UIApplication.shared.setAlternateIconName(topics[nextTopicIndex].1) { error in
            if let error = error {
                print(error.localizedDescription)
            } else {
                print("Success changing app icon!")
            }
        }

        if let url = URL(string: ulyssesURL) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        
    }
    
    // function to get today's date and topic or for another day specified by number of offset days (positive or negative)
    func getTodayDateTopic(daysOffset: Int = 0) -> (String, Int) {
        let date = Date(timeIntervalSinceNow: Double(daysOffset) * day)
        let format = DateFormatter()
        format.dateFormat = "MM/dd/yyyy"
        let formattedDate = format.string(from: date)
//        print(formattedDate)
        
        let calendar = Calendar.current
        let todayDate = calendar.component(.day, from: date)
        
        // repeat 15 topics except on the 31st, go to the 16th topic
        let topicIndex = todayDate <= 15 ? todayDate - 1 : (todayDate == 31 ? 15 : todayDate - 16)
        
//        let todayTopic = topics[topicIndex].0
        return (formattedDate, topicIndex)
        
//        let calendar = Calendar.current
//        calendar.component(.year, from: date)
//        calendar.component(.month, from: date)
//        calendar.component(.day, from: date)
    }

}

